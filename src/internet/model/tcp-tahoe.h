#ifndef TCPTAHOE_H
#define TCPTAHOE_H

#include "tcp-congestion-ops.h"
#include "tcp-rate-ops.h"
#include "tcp-socket-state.h"

namespace ns3
{
class TcpTahoe : public TcpCongestionOps
{
  public:
    /**
     * \brief Get the type ID.
     * \return the object TypeId
     */
    static TypeId GetTypeId();

    TcpTahoe();

    /**
     * \brief Copy constructor.
     * \param sock object to copy.
     */
    TcpTahoe(const TcpTahoe& sock);

    ~TcpTahoe() override;

    std::string GetName() const override;

    void IncreaseWindow(Ptr<TcpSocketState> tcb, uint32_t segmentsAcked) override;
    uint32_t GetSsThresh(Ptr<const TcpSocketState> tcb, uint32_t bytesInFlight) override;
    Ptr<TcpCongestionOps> Fork() override;

  protected:
    virtual uint32_t SlowStart(Ptr<TcpSocketState> tcb, uint32_t segmentsAcked);
    virtual void CongestionAvoidance(Ptr<TcpSocketState> tcb, uint32_t segmentsAcked);
};

} // namespace ns3

#endif // TCPTAHOE_H
